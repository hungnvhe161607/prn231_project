﻿namespace Project_Cinema_PRN231.domain.sysuser
{
    public class CreatedSysUserRequest
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
    }
}
