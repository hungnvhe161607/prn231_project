﻿using Project_Cinema_PRN231.model;

namespace Project_Cinema_PRN231.@interface
{
    public interface IBuildingRepository : IGenericRepository<Building>
    {
    }
}
