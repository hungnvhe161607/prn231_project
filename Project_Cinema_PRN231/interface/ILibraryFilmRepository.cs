﻿using Project_Cinema_PRN231.model;
using Project_Cinema_PRN231.repository;

namespace Project_Cinema_PRN231.@interface
{
    public interface ILibraryFilmRepository : IGenericRepository<LibraryFilm>
    {
    }
}
